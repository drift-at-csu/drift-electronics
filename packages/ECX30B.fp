Element["" "ECX30B.fp" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#						Thickn	 Clear	  Mask
  Pad[	-1675um  1100um -2025um  1100um		1250um    305um    1350um "1" "1" "square"]
  Pad[   1675um  1100um  2025um  1100um         1250um    305um    1350um "2" "2" "square"]
  Pad[   1675um -1100um  2025um -1100um         1250um    305um    1350um "3" "3" "square"]
  Pad[  -1675um -1100um -2025um -1100um         1250um    305um    1350um "4" "4" "square"]

  ElementLine [	 -800um  1600um   800um   1600um 254um]
  ElementLine [  2500um   200um  2500um   -200um 254um]
  ElementLine [   800um -1600um  -800um  -1600um 254um]
  ElementLine [ -2500um  -200um -2500um    200um 254um]

  
)
