Element[0x00000000 "" "" "1_5_0603" 0 0 -3150 -3150 0 100 ""]
(
# 
# Pad[x1, y1, x2, y2, thickness, clearance, mask, name , pad number, flags]

Pad[	-5118 -492
	-5118 492
	2952 2000 3552 "2" "2" "square"]
Pad[	5118 -492
	5118 492
	2952 2000 3552 "3" "3" "square"]
Pad[    0 -492
	0 492
	2952 2000 3552 "1" "1" "square"]

ElementLine [-7100 -2500 -7100 2500 800]
ElementLine [-7100 -2500 2000 -2500 800]
ElementLine [-7100 2500 2000 2500 800]
)
