#
Element["" "CRA06E.fp" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#                                       	Thickn   Clear   Mask
#Pad[	-5000um -7000um -5000um -7000um		510um 304800nm 610unm "1" "1" "square"]
Pad[        0um  -355um     0um   355um		440um	600um	 540um	"1" "1" "square"]
Pad[        0um -2305um     0um -1595um         440um   600um    540um  "2" "2" "square"]
Pad[      800um  -355um   800um   355um         440um   600um    540um  "3" "3" "square"]
Pad[      800um -2305um   800um -1595um         440um   600um    540um  "4" "4" "square"]
Pad[     1600um  -355um  1600um   355um         440um   600um    540um  "5" "5" "square"]
Pad[     1600um -2305um  1600um -1595um         440um   600um    540um  "6" "6" "square"]
Pad[     2400um  -355um  2400um   355um         440um   600um    540um  "7" "7" "square"]
Pad[     2400um -2305um  2400um -1595um         440um   600um    540um  "8" "8" "square"]

ElementLine [-400um -225um -400um -1725um 155um]
ElementLine [2800um -225um 2800um -1725um 155um]
)
