Element["" "RJ45_4G05ETP1" "J?" ""  0 0 0mil -200mil 0 200 ""] (
# Element [element_flags, description, pcb-name, value, 
# 				mark_x, mark_y, text_x, text_y, 
# 				text_direction, text_scale, text_flags]
#				Thick	Clear	Mask	Drill
Pin  [   0mil	-350mil		60mil	20mil	66mil	35mil	"1" "1" "" ]
Pin [   50mil	-250mil		60mil   20mil   66mil   35mil   "2" "2" "" ]
Pin [  100mil   -350mil		60mil   20mil   66mil   35mil   "3" "3" "" ]
Pin [  150mil   -250mil		60mil   20mil   66mil   35mil   "4" "4" "" ]
Pin [  200mil	-350mil		60mil   20mil   66mil   35mil   "5" "5" "" ]
Pin [  250mil	-250mil		60mil   20mil   66mil   35mil   "6" "6" "" ]
Pin [  300mil	-350mil		60mil   20mil   66mil   35mil   "7" "7" "" ]
Pin [  350mil	-250mil		60mil   20mil   66mil   35mil   "8" "8" "" ]
Pin [  400mil   -350mil		60mil   20mil   66mil   35mil   "9" "9" "" ]
Pin [  450mil   -250mil		60mil   20mil   66mil   35mil   "10" "10" "" ]

# LEDs
Pin [    -36mil  160mil		65mil   20mil   71mil   40mil   "D1" "D1" "" ]
Pin [     64mil  160mil         65mil   20mil   71mil   40mil   "D2" "D2" "" ]
Pin [    386mil  160mil         65mil   20mil   71mil   40mil   "D3" "D3" "" ]
Pin [    486mil  160mil         65mil   20mil   71mil   40mil   "D4" "D4" "" ]
# =============== Holes:
#                               Thick   Clear   Mask    Drill
# Allignment
Pin [     0mil     0mil		160mil  20mil   166mil	128mil  "0" "0" "" ]
Pin [    450mil    0mil		160mil  20mil   166mil  128mil  "0" "0" "" ]
# Shielding
Pin [    -80mil	  -120mil	90mil   20mil   96mil   62mil   "0" "0" "" ]
Pin [    530mil	  -120mil	90mil   20mil   96mil   62mil   "0" "0" "" ]

ElementLine [ -98mil  272mil  -98mil  -50mil	10mil]
ElementLine [ -98mil -425mil  -98mil -200mil	10mil]
ElementLine [ 548mil  272mil  548mil  -50mil	10mil]
ElementLine [ 548mil -425mil  548mil -200mil	10mil]

ElementLine [ -98mil -425mil  548mil -425mil    10mil]

# Attribute("description" "Shielded RJ45")
)
