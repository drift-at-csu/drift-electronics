Element["" "RJ54_2by4" "J?" ""  0 0 0mil -200mil 0 200 ""] (
# Element [element_flags, description, pcb-name, value, 
# 				mark_x, mark_y, text_x, text_y, 
# 				text_direction, text_scale, text_flags]
#				Thick	Clear	Mask	Drill
Pin [ 0mil	0mil		60mil	20mil	66mil	35mil	"1T1" "1T1" "" ]
Pin [ -50mil	100mil		60mil   20mil   66mil   35mil   "1T2" "1T2" "" ]
Pin [ -100mil   0mil            60mil   20mil   66mil   35mil   "1T3" "1T3" "" ]
Pin [ -150mil   100mil		60mil   20mil   66mil   35mil   "1T4" "1T4" "" ]
Pin [ -200mil	0mil		60mil   20mil   66mil   35mil   "1T5" "1T5" "" ]
Pin [ -250mil	100mil		60mil   20mil   66mil   35mil   "1T6" "1T6" "" ]
Pin [ -300mil	0mil		60mil   20mil   66mil   35mil   "1T7" "1T7" "" ]
Pin [ -350mil	100mil		60mil   20mil   66mil   35mil   "1T8" "1T8" "" ]
#
Pin [ 0mil	260mil		60mil   20mil   66mil   35mil   "1B8" "1B8" "" ]
Pin [ -50mil	360mil		60mil   20mil   66mil   35mil   "1B7" "1B7" "" ]
Pin [ -100mil	260mil		60mil   20mil   66mil   35mil   "1B6" "1B6" "" ]
Pin [ -150mil	360mil		60mil   20mil   66mil   35mil   "1B5" "1B5" "" ]
Pin [ -200mil	260mil		60mil   20mil   66mil   35mil   "1B4" "1B4" "" ]
Pin [ -250mil	360mil		60mil   20mil   66mil   35mil   "1B3" "1B3" "" ]
Pin [ -300mil	260mil		60mil   20mil   66mil   35mil   "1B2" "1B2" "" ]
Pin [ -350mil	360mil          60mil   20mil   66mil   35mil   "1B1" "1B1" "" ]
# Second Column
Pin [ -550mil	0mil		60mil   20mil   66mil   35mil   "2T1" "2T1" "" ]
Pin [ -600mil	100mil		60mil   20mil   66mil   35mil   "2T2" "2T2" "" ]
Pin [ -650mil	0mil		60mil   20mil   66mil   35mil   "2T3" "2T3" "" ]
Pin [ -700mil	100mil		60mil   20mil   66mil   35mil   "2T4" "2T4" "" ]
Pin [ -750mil	0mil		60mil   20mil   66mil   35mil   "2T5" "2T5" "" ]
Pin [ -800mil	100mil		60mil   20mil   66mil   35mil   "2T6" "2T6" "" ]
Pin [ -850mil	0mil		60mil   20mil   66mil   35mil   "2T7" "2T7" "" ]
Pin [ -900mil	100mil		60mil   20mil   66mil   35mil   "2T8" "2T8" "" ]
#
Pin [ -550mil	260mil		60mil   20mil   66mil   35mil   "2B8" "2B8" "" ]
Pin [ -600mil	360mil		60mil   20mil   66mil   35mil   "2B7" "2B7" "" ]
Pin [ -650mil	260mil		60mil   20mil   66mil   35mil   "2B6" "2B6" "" ]
Pin [ -700mil	360mil		60mil   20mil   66mil   35mil   "2B5" "2B5" "" ]
Pin [ -750mil	260mil		60mil   20mil   66mil   35mil   "2B4" "2B4" "" ]
Pin [ -800mil	360mil		60mil   20mil   66mil   35mil   "2B3" "2B3" "" ]
Pin [ -850mil	260mil		60mil   20mil   66mil   35mil   "2B2" "2B2" "" ]
Pin [ -900mil	360mil		60mil   20mil   66mil   35mil   "2B1" "2B1" "" ]
# Third Column
Pin [ -1100mil	0mil		60mil   20mil   66mil   35mil   "3T1" "3T1" "" ]
Pin [ -1150mil	100mil		60mil   20mil   66mil   35mil   "3T2" "3T2" "" ]
Pin [ -1200mil	0mil		60mil   20mil   66mil   35mil   "3T3" "3T3" "" ]
Pin [ -1250mil	100mil		60mil   20mil   66mil   35mil   "3T4" "3T4" "" ]
Pin [ -1300mil	0mil		60mil   20mil   66mil   35mil   "3T5" "3T5" "" ]
Pin [ -1350mil	100mil		60mil   20mil   66mil   35mil   "3T6" "3T6" "" ]
Pin [ -1400mil	0mil		60mil   20mil   66mil   35mil   "3T7" "3T7" "" ]
Pin [ -1450mil	100mil		60mil   20mil   66mil   35mil   "3T8" "3T8" "" ]
#
Pin [ -1100mil	260mil		60mil   20mil   66mil   35mil   "3B8" "3B8" "" ]
Pin [ -1150mil	360mil		60mil   20mil   66mil   35mil   "3B7" "3B7" "" ]
Pin [ -1200mil	260mil		60mil   20mil   66mil   35mil   "3B6" "3B6" "" ]
Pin [ -1250mil	360mil		60mil   20mil   66mil   35mil   "3B5" "3B5" "" ]
Pin [ -1300mil	260mil		60mil   20mil   66mil   35mil   "3B4" "3B4" "" ]
Pin [ -1350mil	360mil		60mil   20mil   66mil   35mil   "3B3" "3B3" "" ]
Pin [ -1400mil	260mil		60mil   20mil   66mil   35mil   "3B2" "3B2" "" ]
Pin [ -1450mil	360mil		60mil   20mil   66mil   35mil   "3B1" "3B1" "" ]
# Fourth Column
Pin [ -1650mil	0mil		60mil   20mil   66mil   35mil   "4T1" "4T1" "" ]
Pin [ -1700mil	100mil		60mil   20mil   66mil   35mil   "4T2" "4T2" "" ]
Pin [ -1750mil	0mil		60mil   20mil   66mil   35mil   "4T3" "4T3" "" ]
Pin [ -1800mil	100mil		60mil   20mil   66mil   35mil   "4T4" "4T4" "" ]
Pin [ -1850mil	0mil		60mil   20mil   66mil   35mil   "4T5" "4T5" "" ]
Pin [ -1900mil	100mil		60mil   20mil   66mil   35mil   "4T6" "4T6" "" ]
Pin [ -1950mil	0mil		60mil   20mil   66mil   35mil   "4T7" "4T7" "" ]
Pin [ -2000mil	100mil		60mil   20mil   66mil   35mil   "4T8" "4T8" "" ]
#
Pin [ -1650mil	260mil		60mil   20mil   66mil   35mil   "4B8" "4B8" "" ]
Pin [ -1700mil	360mil		60mil   20mil   66mil   35mil   "4B7" "4B7" "" ]
Pin [ -1750mil	260mil		60mil   20mil   66mil   35mil   "4B6" "4B6" "" ]
Pin [ -1800mil	360mil		60mil   20mil   66mil   35mil   "4B5" "4B5" "" ]
Pin [ -1850mil	260mil		60mil   20mil   66mil   35mil   "4B4" "4B4" "" ]
Pin [ -1900mil	360mil		60mil   20mil   66mil   35mil   "4B3" "4B3" "" ]
Pin [ -1950mil	260mil		60mil   20mil   66mil   35mil   "4B2" "4B2" "" ]
Pin [ -2000mil	360mil		60mil   20mil   66mil   35mil   "4B1" "4B1" "" ]
# =============== Holes:
#                               Thick   Clear   Mask    Drill
# Allignment
Pin [ -2050mil  610mil		160mil  20mil   166mil	128mil  "0" "0" "" ]
Pin [    50mil  610mil		160mil  20mil   166mil  128mil  "0" "0" "" ]
# External Shielding
# Pin [ -2160mil  790mil          90mil	20mil	96mil	62mil	"0" "0" "" ]
Pin [ -2160mil	  0mil		90mil   20mil   96mil   62mil   "0" "0" "" ]
# Pin [   160mil	790mil		90mil   20mil   96mil   62mil   "0" "0" "" ]
Pin [   160mil	160mil		90mil   20mil   96mil   62mil   "0" "0" "" ]
# Internal Shielding
Pin [ -2130mil  250mil		90mil   20mil   96mil   62mil   "0" "0" "" ]
Pin [   130mil  380mil		90mil   20mil   96mil   62mil   "0" "0" "" ]

ElementLine [ -2160mil -95mil 160mil -95mil 10mil]

# Attribute("description" "Shielded RJ45")
)
