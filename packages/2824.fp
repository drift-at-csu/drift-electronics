Element["" "2824" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#					Thick	Clear 	Mask
Pad[  107mil  -72mil  107mil	72mil 	96mil  20mil   	100mil "1" "1" "square"]
Pad[  -93mil   -86mil -93mil 	86mil	68mil  20mil 	72mil  "2" "2" "square"]

# There is no outline needed ?
ElementLine [  -50mil  120mil  50mil 120mil	6mil]
ElementLine [  -50mil  -120mil  50mil -120mil     6mil]

)
