Element["" "TPBGA24b05.fp" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#					Thickn	 Clear	  Mask
# Pad[	-2000um	-2000um	-2000um	-2000um	510000nm 304800nm 610000nm "A1" "A1" ""]
  Pad[	-1000um -2000um -1000um -2000um	510000nm 304800nm 610000nm "A2" "A2" ""]
  Pad[	    0um	-2000um	    0um	-2000um	510000nm 304800nm 610000nm "A3" "A3" ""]
  Pad[   1000um -2000um  1000um -2000um 510000nm 304800nm 610000nm "A4" "A4" ""]
  Pad[   2000um -2000um  2000um -2000um 510000nm 304800nm 610000nm "A5" "A5" ""]

  Pad[  -2000um -1000um -2000um -1000um 510000nm 304800nm 610000nm "B1" "B1" ""]
  Pad[  -1000um -1000um -1000um -1000um 510000nm 304800nm 610000nm "B2" "B2" ""]
  Pad[      0um -1000um     0um -1000um 510000nm 304800nm 610000nm "B3" "B3" ""]
  Pad[   1000um -1000um  1000um -1000um 510000nm 304800nm 610000nm "B4" "B4" ""]
  Pad[   2000um -1000um  2000um -1000um 510000nm 304800nm 610000nm "B5" "B5" ""]

  Pad[  -2000um     0um -2000um     0um 510000nm 304800nm 610000nm "C1" "C1" ""]
  Pad[  -1000um     0um -1000um     0um 510000nm 304800nm 610000nm "C2" "C2" ""]
  Pad[      0um     0um     0um     0um 510000nm 304800nm 610000nm "C3" "C3" ""]
  Pad[   1000um     0um  1000um     0um 510000nm 304800nm 610000nm "C4" "C4" ""]
  Pad[   2000um     0um  2000um     0um 510000nm 304800nm 610000nm "C5" "C5" ""]

  Pad[  -2000um  1000um -2000um  1000um 510000nm 304800nm 610000nm "D1" "D1" ""]
  Pad[  -1000um  1000um -1000um  1000um 510000nm 304800nm 610000nm "D2" "D2" ""]
  Pad[      0um  1000um     0um  1000um 510000nm 304800nm 610000nm "D3" "D3" ""]
  Pad[   1000um  1000um  1000um  1000um 510000nm 304800nm 610000nm "D4" "D4" ""]
  Pad[   2000um  1000um  2000um  1000um 510000nm 304800nm 610000nm "D5" "D5" ""]

  Pad[  -2000um  2000um -2000um  2000um 510000nm 304800nm 610000nm "E1" "E1" ""]
  Pad[  -1000um  2000um -1000um  2000um 510000nm 304800nm 610000nm "E2" "E2" ""]
  Pad[      0um  2000um     0um  2000um 510000nm 304800nm 610000nm "E3" "E3" ""]
  Pad[   1000um  2000um  1000um  2000um 510000nm 304800nm 610000nm "E4" "E4" ""]
  Pad[   2000um  2000um  2000um  2000um 510000nm 304800nm 610000nm "E5" "E5" ""]

  ElementLine [	-3000um -4000um  3000um  -4000um 254um]
  ElementLine [ -3000um  4000um  3000um   4000um 254um]
  ElementLine [ -3000um -4000um -3000um   4000um 254um]
  ElementLine [  3000um -4000um  3000um   4000um 254um]

  ElementLine [ -3000um -3000um  -2000um  -4000um 254um]

)
