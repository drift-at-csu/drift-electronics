Element["" "CON_HDR_2mm_28pos_2row" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#					Thickn Clear Mask
# Pad[  -200um  1170um  200um  1170um	2100um 600um 2200um "0" "0" "square"]
#			Thick	Clear 	Mask	Drill
Pin [ -1000um -13000um  1300um  460um   1350um  780um "1" "1" "" ]
Pin [  1000um -13000um  1300um  460um   1350um  780um "2" "2" "" ]
Pin [ -1000um -11000um  1300um  460um   1350um  780um "3" "3" "" ]
Pin [  1000um -11000um  1300um  460um   1350um  780um "4" "4" "" ]
Pin [ -1000um  -9000um	1300um	460um	1350um	780um "5" "5" "" ]
Pin [  1000um  -9000um  1300um  460um   1350um  780um "6" "6" "" ]
Pin [ -1000um  -7000um  1300um  460um   1350um  780um "7" "7" "" ]
Pin [  1000um  -7000um  1300um  460um   1350um  780um "8" "8" "" ]
Pin [ -1000um  -5000um  1300um  460um   1350um  780um "9" "9" "" ]
Pin [  1000um  -5000um  1300um  460um   1350um  780um "10" "10" "" ]
Pin [ -1000um  -3000um  1300um  460um   1350um  780um "11" "11" "" ]
Pin [  1000um  -3000um  1300um  460um   1350um  780um "12" "12" "" ]
Pin [ -1000um  -1000um  1300um  460um   1350um  780um "13" "13" "" ]
Pin [  1000um  -1000um  1300um  460um   1350um  780um "14" "14" "" ]

Pin [ -1000um   1000um  1300um  460um   1350um  780um "15" "15" "" ]
Pin [  1000um   1000um  1300um  460um   1350um  780um "16" "16" "" ]
Pin [ -1000um   3000um  1300um  460um   1350um  780um "17" "17" "" ]
Pin [  1000um   3000um  1300um  460um   1350um  780um "18" "18" "" ]
Pin [ -1000um   5000um  1300um  460um   1350um  780um "19" "19" "" ]
Pin [  1000um   5000um  1300um  460um   1350um  780um "20" "20" "" ]
Pin [ -1000um   7000um  1300um  460um   1350um  780um "21" "21" "" ]
Pin [  1000um   7000um  1300um  460um   1350um  780um "22" "22" "" ]
Pin [ -1000um   9000um  1300um  460um   1350um  780um "23" "23" "" ]
Pin [  1000um   9000um  1300um  460um   1350um  780um "24" "24" "" ]
Pin [ -1000um  11000um  1300um  460um   1350um  780um "25" "25" "" ]
Pin [  1000um  11000um  1300um  460um   1350um  780um "26" "26" "" ]
Pin [ -1000um  13000um  1300um  460um   1350um  780um "27" "27" "" ]
Pin [  1000um  13000um  1300um  460um   1350um  780um "28" "28" "" ]



# There is no outline needed ?
ElementLine [-2000um -13950um  -2000um  13950um	152um]
ElementLine [ 2000um -13950um   2000um  13950um 152um]
ElementLine [-2000um -13950um   2000um -13950um 152um]
ElementLine [-2000um  13950um   2000um  13950um 152um]

ElementLine [-2500um -13950um  -2000um -14450um 152um]
ElementLine [-2500um -13950um  -2000um -13950um 152um]
ElementLine [-2000um -14450um  -2000um -13950um 152um]

)
