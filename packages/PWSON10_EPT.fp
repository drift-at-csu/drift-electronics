Element["" "PWSON10_EPT" "" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#	Pad 0 is an exposed Pad
#					Thickn	Clear	Mask
	
	Pad[ 0um  300um  0um  -300um  	900um	20mil	1000um "0" "0" "square"]
#	The following are thermal vias
#					Thick	Clear 	Mask	Drill
#	Pin [ 0um   -400um		700um   600um	0um	381um "0" "0" "" ]
#	Pin [ 0um    400um		700um   600um	0um	381um "0" "0" "" ]
#						Thickn	Clear	Mask
	Pad[ -1285um -800um -815um -800um 	230um	20mil	330um "1" "1" "square"]
	Pad[ -1285um -400um -815um -400um	230um   20mil   330um "2" "2" "square"]
	Pad[ -1285um    0um -815um    0um	230um   20mil   330um "3" "3" "square"]
	Pad[ -1285um  400um -815um  400um	230um   20mil   330um "4" "4" "square"]
	Pad[ -1285um  800um -815um  800um	230um   20mil   330um "5" "5" "square"]
	Pad[  1285um  800um  815um  800um	230um   20mil   330um "6" "6" "square"]
	Pad[  1285um  400um  815um  400um	230um   20mil   330um "7" "7" "square"]
	Pad[  1285um    0um  815um    0um	230um   20mil   330um "8" "8" "square"]
	Pad[  1285um -400um  815um -400um	230um   20mil   330um "9" "9" "square"]
	Pad[  1285um -800um  815um -800um	230um   20mil   330um "10" "10" "square"]

	ElementLine [-1050um -1050um 1050um  -1050um 	6mil]
	ElementLine [-1050um  1050um 1050um   1050um    6mil]
	ElementLine [-1050um -1050um -1050um -1350um    6mil]
)
