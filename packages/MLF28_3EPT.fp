Element["Switching voltage regulator" "MLF28_3EPT" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#	Pad 0 is an exposed Pad
#					Thickn Clear Mask
Pad[  -200um  1170um  200um  1170um	2100um 600um 2200um "0" "0" "square"]
#	The following are thermal vias
#				Thick	Clear 	Mask	Drill
Pin [  -900um       570um	700um   600um   0um     381um "0" "0" "" ]
Pin [     0um       570um	700um   600um	0um	381um "0" "0" "" ]
Pin [   900um       570um	700um   600um   0um     381um "0" "0" "" ]
Pin [  -900um      1770um       700um   600um   0um     381um "0" "0" "" ]
Pin [     0um      1770um       700um   600um   0um     381um "0" "0" "" ]
Pin [   900um      1770um       700um   600um   0um     381um "0" "0" "" ]

# Left Central Pad
Pad[  -875um  -2005um -875um -1255um	750um 600um 850um "29" "29" "square"]
# Right Central Pad
Pad[   875um  -2005um  875um -1005um    750um 600um 850um "30" "30" "square"]

# Side Pads
#					Thick Clear Mask
# Left
Pad[ -1975um -2755um -1725um -2755um	250um 600um 350um "1" "1" "square"]
Pad[ -1975um -2255um -1725um -2255um    250um 600um 350um "2" "2" "square"]
Pad[ -1975um -1755um -1725um -1755um	250um 600um 350um "3" "3" "square"]
Pad[ -1975um -1255um -1725um -1255um	250um 600um 350um "4" "4" "square"]
Pad[ -1975um  -755um -1725um  -755um	250um 600um 350um "5" "5" "square"]
Pad[ -1975um  -255um -1725um  -255um	250um 600um 350um "6" "6" "square"]
Pad[ -1975um   245um -1725um   245um	250um 600um 350um "7" "7" "square"]
Pad[ -1975um   745um -1725um   745um	250um 600um 350um "8" "8" "square"]
Pad[ -1975um  1245um -1725um  1245um    250um 600um 350um "9" "9" "square"]
Pad[ -1975um  1745um -1725um  1745um    250um 600um 350um "10" "10" "square"]
Pad[ -1975um  2245um -1725um  2245um    250um 600um 350um "11" "11" "square"]

# Right
Pad[  1975um -2755um  1725um -2755um	250um 600um 350um "28" "28" "square"]
Pad[  1975um -2255um  1725um -2255um	250um 600um 350um "27" "27" "square"]
Pad[  1975um -1755um  1725um -1755um	250um 600um 350um "26" "26" "square"]
Pad[  1975um -1255um  1725um -1255um	250um 600um 350um "25" "25" "square"]
Pad[  1975um  -755um  1725um  -755um	250um 600um 350um "24" "24" "square"]
Pad[  1975um  -255um  1725um  -255um	250um 600um 350um "23" "23" "square"]
Pad[  1975um   245um  1725um   245um	250um 600um 350um "22" "22" "square"]
Pad[  1975um   745um  1725um   745um	250um 600um 350um "21" "21" "square"]
Pad[  1975um  1245um  1725um  1245um	250um 600um 350um "20" "20" "square"]
Pad[  1975um  1745um  1725um  1745um	250um 600um 350um "19" "19" "square"]
Pad[  1975um  2245um  1725um  2245um	250um 600um 350um "18" "18" "square"]

# Bottom
Pad[ -1250um  2725um -1250um  2975um	250um 600um 350um "12" "12" "square"]
Pad[  -750um  2725um  -750um  2975um	250um 600um 350um "13" "13" "square"]
Pad[  -250um  2725um  -250um  2975um	250um 600um 350um "14" "14" "square"]
Pad[   250um  2725um   250um  2975um	250um 600um 350um "15" "15" "square"]
Pad[   750um  2725um   750um  2975um	250um 600um 350um "16" "16" "square"]
Pad[  1250um  2725um  1250um  2975um    250um 600um 350um "17" "17" "square"]

# Outline

ElementLine [-2000um -3000um  2000um -3000um	152um]

ElementLine [-2000um  3000um -1640um  3000um	152um]
ElementLine [-2000um  3000um -2000um  2640um	152um]

ElementLine [ 2000um  3000um  1640um  3000um    152um]
ElementLine [ 2000um  3000um  2000um  2640um    152um]
)
