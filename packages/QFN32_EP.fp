Element["" "QFN32_EP" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#	Pad 0 is an exposed Pad
#							  Thickn    Clear     Mask
	Pad[ -862500nm   862500nm  862500nm  862500nm  1725000nm 600000nm 1825000nm "0" "0" "square"]
	Pad[ -862500nm  -862500nm  862500nm -862500nm  1725000nm 600000nm 1825000nm "0" "0" "square"]
#	The following are thermal vias
#					Thick	Clear 	Mask	Drill
	Pin [ -1000um   -1000um		890um   600um	0um	381um "0" "0" "" ]
	Pin [     0um	-1000um		890um	600um	0um	381um "0" "0" "" ]
	Pin [  1000um   -1000um		890um	600um	0um	381um "0" "0" "" ]

	Pin [ -1000um       0um		890um	600um	0um     381um "0" "0" "" ]
	Pin [     0um       0um		890um	600um	0um     381um "0" "0" "" ]
	Pin [  1000um       0um		890um	600um	0um     381um "0" "0" "" ]

	Pin [ -1000um   1000um		890um	600um	0um     381um "0" "0" "" ]
	Pin [     0um   1000um		890um	600um	0um     381um "0" "0" "" ]
	Pin [  1000um   1000um		890um	600um	0um     381um "0" "0" "" ]

#						Thickn	 Clear	Mask
	Pad[-2900um -1750um -2190um -1750um 	280um	600um	380um "1" "1" ""]
	Pad[-2900um -1250um -2190um -1250um     280um   600um   380um "2" "2" ""]
	Pad[-2900um  -750um -2190um  -750um     280um   600um   380um "3" "3" ""]
	Pad[-2900um  -250um -2190um  -250um     280um   600um   380um "4" "4" ""]
	Pad[-2900um   250um -2190um   250um     280um   600um   380um "5" "5" ""]
	Pad[-2900um   750um -2190um   750um     280um   600um   380um "6" "6" ""]
	Pad[-2900um  1250um -2190um  1250um     280um   600um   380um "7" "7" ""]
	Pad[-2900um  1750um -2190um  1750um     280um   600um   380um "8" "8" ""]

	Pad[-1750um  2900um -1750um  2190um	280um   600um   380um "9" "9" ""]
	Pad[-1250um  2900um -1250um  2190um     280um   600um   380um "10" "10" ""]
	Pad[ -750um  2900um  -750um  2190um     280um   600um   380um "11" "11" ""]
	Pad[ -250um  2900um  -250um  2190um     280um   600um   380um "12" "12" ""]
	Pad[  250um  2900um   250um  2190um     280um   600um   380um "13" "13" ""]
	Pad[  750um  2900um   750um  2190um     280um   600um   380um "14" "14" ""]
	Pad[ 1250um  2900um  1250um  2190um     280um   600um   380um "15" "15" ""]
	Pad[ 1750um  2900um  1750um  2190um     280um   600um   380um "16" "16" ""]

	Pad[ 2900um  1750um  2190um  1750um     280um   600um   380um "17" "17" ""]
	Pad[ 2900um  1250um  2190um  1250um     280um   600um   380um "18" "18" ""]
	Pad[ 2900um   750um  2190um   750um     280um   600um   380um "19" "19" ""]
	Pad[ 2900um   250um  2190um   250um     280um   600um   380um "20" "20" ""]
	Pad[ 2900um  -250um  2190um  -250um     280um   600um   380um "21" "21" ""]
	Pad[ 2900um  -750um  2190um  -750um     280um   600um   380um "22" "22" ""]
	Pad[ 2900um -1250um  2190um -1250um     280um   600um   380um "23" "23" ""]
	Pad[ 2900um -1750um  2190um -1750um     280um   600um   380um "24" "24" ""]

	Pad[ 1750um -2900um  1750um -2190um     280um   600um   380um "25" "25" ""]
	Pad[ 1250um -2900um  1250um -2190um     280um   600um   380um "26" "26" ""]
	Pad[  750um -2900um   750um -2190um     280um   600um   380um "27" "27" ""]
	Pad[  250um -2900um   250um -2190um     280um   600um   380um "28" "28" ""]
	Pad[ -250um -2900um  -250um -2190um     280um   600um   380um "29" "29" ""]
	Pad[ -750um -2900um  -750um -2190um     280um   600um   380um "30" "30" ""]
	Pad[-1250um -2900um -1250um -2190um     280um   600um   380um "31" "31" ""]
	Pad[-1750um -2900um -1750um -2190um     280um   600um   380um "32" "32" ""]

	ElementLine [-2575um -2575um -2075um -2575um 152um]
	ElementLine [ 2575um -2575um  2075um -2575um 152um]

	ElementLine [ 2575um -2575um  2575um -2075um 152um]
	ElementLine [ 2575um  2575um  2575um  2075um 152um]

	ElementLine [ 2575um  2575um  2075um  2575um 152um]
	ElementLine [-2575um  2575um -2075um  2575um 152um]

	ElementLine [-2575um  2575um -2575um  2075um 152um]
	ElementLine [-2575um -2575um -2575um -2075um 152um]

	ElementLine [-2575um -2075um -2075um -2575um 152um]
)
