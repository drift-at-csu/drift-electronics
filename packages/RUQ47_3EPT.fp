Element["" "RUQ47_3EPT" "Name" "Val" 1000 1000 0nm -1000000nm 0 100 ""] (
#	Pad 0 is an exposed Pad
#					Thickn Clear Mask
# Pad[  -200um  1170um  200um  1170um	2100um 600um 2200um "0" "0" "square"]
#	The following are thermal vias
#			Thick	Clear 	Mask	Drill
Pin [ -1380um -1380um	700um   600um   0um     381um "45" "45" "" ]
Pin [ -1380um  -690um   700um   600um   0um     381um "45" "45" "" ]
Pin [  -690um -1380um   700um   600um   0um     381um "45" "45" "" ]
Pin [     0um -1380um   700um   600um   0um     381um "45" "45" "" ]
Pin [   690um -1380um   700um   600um   0um     381um "45" "45" "" ]
Pin [  1380um  -690um   700um   600um   0um     381um "45" "45" "" ]
Pin [  1380um -1380um   700um   600um   0um     381um "45" "45" "" ]

Pin [ -1380um  1380um   700um   600um   0um     381um "45" "45" "" ]
Pin [ -1380um   690um   700um   600um   0um     381um "45" "45" "" ]
Pin [  -690um  1380um   700um   600um   0um     381um "45" "45" "" ]
Pin [     0um  1380um   700um   600um   0um     381um "45" "45" "" ]
Pin [   690um  1380um   700um   600um   0um     381um "45" "45" "" ]
Pin [  1380um   690um   700um   600um   0um     381um "45" "45" "" ]
Pin [  1380um  1380um   700um   600um   0um     381um "45" "45" "" ]


# Corner Pad #1 is special
Pad[ -3900um -3000um  -4100um -3200um	 848um 600um  948um "1" "1" "square"]
Pad[ -4400um -2900um  -4400um -3800um	1000um 600um 1100um "1" "1" "square"]
Pad[ -3600um -3300um  -3600um -4000um	 600um 600um  700um "1" "1" "square"]

# 3 other Corner Pads:
Pad[ -4100um 10400um  -4100um 10700um	1600um 600um 1700um "15" "15" "square"]
Pad[  4100um 10400um   4100um 10700um   1600um 600um 1700um "23" "23" "square"]
Pad[  4100um -3500um   4100um -3200um   1600um 600um 1700um "37" "37" "square"]
# Top Central Pad (45)
Pad[  -513um  -513um    513um  -513um	1026um 600um 1126um "45" "45" "square"]
Pad[  -513um   513um    513um   513um   1026um 600um 1126um "45" "45" "square"]
Pad[ -2700um     0um   2700um     0um    600um 600um  700um "45" "45" "square"]

# Bottom Central Pads
Pad[ -2475um  7925um  -2475um  6025um	2200um 600um 2300um "46" "46" "square"]
Pad[  2475um  7925um   2475um  6025um   2200um 600um 2300um "47" "47" "square"]

# Side Pads: Left
#					Thick Clear Mask
Pad[ -4595um -1800um -4245um -1800um	600um 600um 700um "2" "2" "square"]
Pad[ -4595um  -900um -4245um  -900um	600um 600um 700um "3" "3" "square"]
Pad[ -4595um     0um -4245um     0um	600um 600um 700um "4" "4" "square"]
Pad[ -4595um   900um -4245um   900um	600um 600um 700um "5" "5" "square"]
Pad[ -4595um  1800um -4245um  1800um	600um 600um 700um "6" "6" "square"]
Pad[ -4595um  2700um -4245um  2700um	600um 600um 700um "7" "7" "square"]
Pad[ -4595um  3600um -4245um  3600um	600um 600um 700um "8" "8" "square"]
Pad[ -4595um  4500um -4245um  4500um	600um 600um 700um "9" "9" "square"]
Pad[ -4595um  5400um -4245um  5400um	600um 600um 700um "10" "10" "square"]
Pad[ -4595um  6300um -4245um  6300um	600um 600um 700um "11" "11" "square"]
Pad[ -4595um  7200um -4245um  7200um	600um 600um 700um "12" "12" "square"]
Pad[ -4595um  8100um -4245um  8100um	600um 600um 700um "13" "13" "square"]
Pad[ -4595um  9000um -4245um  9000um	600um 600um 700um "14" "14" "square"]
# Side Pads: Bottom
Pad[ -2700um 11200um -2700um 10850um	600um 600um 700um "16" "16" "square"]
Pad[ -1800um 11200um -1800um 10850um	600um 600um 700um "17" "17" "square"]
Pad[  -900um 11200um  -900um 10850um	600um 600um 700um "18" "18" "square"]
Pad[     0um 11200um     0um 10850um	600um 600um 700um "19" "19" "square"]
Pad[   900um 11200um   900um 10850um	600um 600um 700um "20" "20" "square"]
Pad[  1800um 11200um  1800um 10850um	600um 600um 700um "21" "21" "square"]
Pad[  2700um 11200um  2700um 10850um	600um 600um 700um "22" "22" "square"]
# Side Pads: Right
Pad[  4595um -1800um  4245um -1800um	600um 600um 700um "36" "36" "square"]
Pad[  4595um  -900um  4245um  -900um	600um 600um 700um "35" "35" "square"]
Pad[  4595um     0um  4245um     0um	600um 600um 700um "34" "34" "square"]
Pad[  4595um   900um  4245um   900um	600um 600um 700um "33" "33" "square"]
Pad[  4595um  1800um  4245um  1800um	600um 600um 700um "32" "32" "square"]
Pad[  4595um  2700um  4245um  2700um	600um 600um 700um "31" "31" "square"]
Pad[  4595um  3600um  4245um  3600um	600um 600um 700um "30" "30" "square"]
Pad[  4595um  4500um  4245um  4500um	600um 600um 700um "29" "29" "square"]
Pad[  4595um  5400um  4245um  5400um	600um 600um 700um "28" "28" "square"]
Pad[  4595um  6300um  4245um  6300um	600um 600um 700um "27" "27" "square"]
Pad[  4595um  7200um  4245um  7200um	600um 600um 700um "26" "26" "square"]
Pad[  4595um  8100um  4245um  8100um	600um 600um 700um "25" "25" "square"]
Pad[  4595um  9000um  4245um  9000um	600um 600um 700um "24" "24" "square"]
# Side Pads: Top
Pad[ -2700um -3650um -2700um -4000um    600um 600um 700um "44" "44" "square"]
Pad[ -1800um -3650um -1800um -4000um    600um 600um 700um "43" "43" "square"]
Pad[  -900um -3650um  -900um -4000um    600um 600um 700um "42" "42" "square"]
Pad[     0um -3650um     0um -4000um    600um 600um 700um "41" "41" "square"]
Pad[   900um -3650um   900um -4000um    600um 600um 700um "40" "40" "square"]
Pad[  1800um -3650um  1800um -4000um    600um 600um 700um "39" "39" "square"]
Pad[  2700um -3650um  2700um -4000um    600um 600um 700um "38" "38" "square"]

# There is no outline needed ?
# ElementLine [-2000um -3000um  2000um -3000um	152um]

)
