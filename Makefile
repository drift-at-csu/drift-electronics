ANALOG_SCH_FILES  = drift_accumulator_01_analog_adc1.sch
ANALOG_SCH_FILES += drift_accumulator_02_analog_adc2.sch
ANALOG_SCH_FILES += drift_accumulator_03_analog_adc3.sch
ANALOG_SCH_FILES += drift_accumulator_04_analog_adc4.sch
ANALOG_SCH_FILES += drift_accumulator_05_analog_adc5.sch
ANALOG_SCH_FILES += drift_accumulator_06_analog_adc6.sch
ANALOG_SCH_FILES += drift_accumulator_07_analog_feconnector.sch
ANALOG_SCH_FILES += drift_accumulator_08_digital_adcs.sch
ANALOG_SCH_FILES += drift_accumulator_09_mem1.sch
ANALOG_SCH_FILES += drift_accumulator_10_mem2.sch
ANALOG_SCH_FILES += drift_accumulator_11_eth1.sch
ANALOG_SCH_FILES += drift_accumulator_12_config.sch
ANALOG_SCH_FILES += drift_accumulator_13_power1.sch
ANALOG_SCH_FILES += drift_accumulator_14_power2.sch
ANALOG_SCH_FILES += drift_accumulator_15_power_pld.sch
ANALOG_SCH_FILES += drift_accumulator_16_qsfp.sch
all: drift_accumulator.pcb drift_knot.pcb

drift_accumulator.pcb: $(ANALOG_SCH_FILES)
	gsch2pcb $(ANALOG_SCH_FILES) -o drift_accumulator

drift_knot.pcb: drift_knot_01.sch drift_knot_02.sch
	gsch2pcb drift_knot_01.sch drift_knot_02.sch -o drift_knot
